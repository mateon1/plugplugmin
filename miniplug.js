// PlugPlugMin v0.1
// etnrisouaflchpdvmgybwESxTNCkLAOM_DPHBjFIqRUzWXV$JKQGYZ0516372984
(function (
	e,//API
	t,//true
	n //false
) {
	var r = t,//Autowoot
		i = n;//Autojoin
	e.chatLog("Loaded PlugPlugMin v0.1");
	e.on(e.CHAT, function (t) {
		(t.message.indexOf("@" + e.getUser().username) != -1
				&& (i && /!disable/.test(t.message) && e.hasPermission(t.fromID, 1) && (e.sendChat("@" + t.from + " - Autojoin disabled") || (i = !i))
				|| /!status/.test(t.message) && e.sendChat("@" + t.from + " - Status: Running PlugPlugMin v0.1, autowoot: " + r + ", autojoin: " + i)));
	});
	e.on(e.CHAT_COMMAND, function (t) {
		t == "/w" && e.chatLog("Autowoot turned o" + ((r = !r) ? "n" : "ff"))
				|| t == "/j" && e.chatLog("Autojoin turned o" + ((i = !i) ? "n" : "ff"));
	});
	e.on(e.DJ_ADVANCE, function (t) {
		(r && $("#woot").click());
		(i && t.lastPlay.dj.id == e.getUser().id && e.djJoin())
	})
})(API, !0, !1)