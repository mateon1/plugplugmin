A lightweight script for Plug.dj

** Download by bookmarking the code below and clicking the bookmark every time you load Plug **

```
#!javascript

javascript:$.getScript("https://bitbucket.org/mateon1/plugplugmin/downloads/minified.js");
```

It's only features are:

* Autojoin with !disable, toggled by "/j" in chat
* Autowoot toggled by "/w" in chat

The script probably won't be updated unless bugs are found.

** REPORT BUGS TO THE 'ISSUES' TAB **
